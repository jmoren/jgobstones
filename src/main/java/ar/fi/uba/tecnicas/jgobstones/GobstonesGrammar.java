package ar.fi.uba.tecnicas.jgobstones;

import org.petitparser.parser.Parser;
import org.petitparser.parser.characters.CharacterParser;
import org.petitparser.parser.combinators.ChoiceParser;
import org.petitparser.parser.combinators.SettableParser;
import org.petitparser.parser.primitive.FailureParser;
import org.petitparser.tools.CompositeParser;
import org.petitparser.tools.ExpressionBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.petitparser.parser.characters.CharacterParser.*;
import static org.petitparser.parser.combinators.SettableParser.undefined;
import static org.petitparser.parser.primitive.StringParser.of;

public class GobstonesGrammar extends CompositeParser {

    public static final Parser OPEN_PARENS = is('(').trim();
    public static final Parser CLOSE_PARENS = is(')').trim();
    public static final Parser COMA = is(',').trim();

    @Override
    protected void initialize() {
        def("start", ref("program"));

        def("program", ref("color").seq(ref("reservedOp").seq(ref("color"))
        ));
//                .or(ref("number")
//                        .or(ref("boolean"))
//                        .or(ref("color"))
//                        .or(ref("direction"))
//                        .or(ref("predefProc"))
//                        .or(ref("predefFunc"))
//                        .or(ref("reservedOp"))));

        def("positiveNumber", positiveNumber());
        def("number", number());
        def("comment", comment());
        def("whitespace", whitespace());
        def("boolean", booleans());
        def("color", color());
        def("direction", direction());
        def("literal", literals());

        def("gexp", gexp());
        def("simpleCmd", simpleCmd());


        def("lowerId", lowerId());
        def("upperId", upperId());

        def("varTuple", varTuples());
        def("funcName", lowerId());
        def("varName", lowerId());


        def("predefProc", predefinedProcedures());
        def("predefFunc", predefinedFunctions());
        def("reservedOp", reservedOp());
    }

    private Parser simpleCmd() {
        Parser simpleAssignment = lowerId().seq(token(":=")).seq(ref("gexp"));
        Parser lastVar = is(',').trim().seq(lowerId()).star();
        Parser multiAssignment = is('(').trim().seq(lowerId()).seq(lastVar).seq(is(')').trim()).seq(token(":=")).seq(ref("gexp"));

        Parser procCall = upperId().seq(OPEN_PARENS).seq(ref("gexpTuple").optional()).seq(CLOSE_PARENS);
        return simpleAssignment.or(multiAssignment).or(token("Skip")).or(procCall);
    }

    private Parser gexp() {
        SettableParser gexp = undefined();

        Parser gexpTuple = gexp.trim().seq(COMA.seq(gexp).star());
        def("gexpTuple", gexpTuple);

        ExpressionBuilder builder = new ExpressionBuilder();
        builder.group()
                .primitive(OPEN_PARENS.seq(gexp).seq(CLOSE_PARENS).pick(1))
                .primitive(is('-').optional().seq(digit().seq(digit().star())).trim())
                .primitive(token("True").or(token("False")).trim())
                .primitive(token("Norte").or(token("Este").or(token("Sur")).or(token("Oeste"))).trim())
                .primitive(token("Verde").or(token("Rojo").or(token("Azul")).or(token("Negro"))).trim())
                .primitive(lowerId().seq(OPEN_PARENS.seq(gexpTuple.optional()).seq(CLOSE_PARENS)))
        ;

        builder.group()
                .prefix(token("not"), (List<Object> values) -> buildNotExpression(values.get(1)));
        builder.group()
                .right(is('^').trim(), (List<Object> values) -> buildPowExpression(values.get(0), values.get(2)));
        builder.group()
                .left(bops(), (List<Object> values) -> buildBoolExpression(values.get(0), values.get(2)));
        builder.group()
                .left(rops(), (List<Object> values) -> buildRopExpression(values.get(0), values.get(2)));
        builder.group()
                .left(nops(), (List<Object> values) -> buildNopExpression(values.get(0), values.get(2)));
        builder.group()
                .left(mops(), (List<Object> values) -> buildMopExpression(values.get(0), values.get(2)));
        gexp.setDelegate(builder.build());
        return gexp.end();
    }

    private Void buildPowExpression(Object o, Object o1) {
        return null;
    }

    Void buildNotExpression(Object a) {
        return null;
    }

    Void buildBoolExpression(Object a, Object b) {
        return null;
    }


    Void buildRopExpression(Object a, Object b) {
        return null;
    }

    Void buildNopExpression(Object a, Object b) {
        return null;
    }

    Void buildMopExpression(Object a, Object b) {
        return null;
    }

    private Parser varTuples() {
        Parser lastVar = is(',').trim().seq(lowerId()).star();
        return of("()")
                .or(is('(').trim().seq(lowerId()).seq(lastVar).seq(is(')').trim()));
    }

    private Parser bops() {
        return newLiterals("||", "&&");
    }

    private Parser rops() {
        return newLiterals("==", "/=", "<=", "<", ">=", ">");
    }

    private Parser nops() {
        return newLiterals("+", "-");
    }

    private Parser mops() {
        return newLiterals("mod", "div");
    }

    private Parser lowerId() {
        return lowerCase().seq(word().star()).trim();
    }

    private Parser upperId() {
        return upperCase().seq(word().star()).trim();
    }

    private Parser literals() {
        return ref("number")
                .or(ref("color")
                        .or(ref("direction")
                                .or(ref("boolean")
                                        .or(FailureParser.withMessage("Literal expected.")))));
    }

    protected Parser comment() {
        return multiLineComment("{-", "-}")
                .or(multiLineComment("/*", "*/"))
                .or(multiLineComment("\"\"\"", "\"\"\""))
                .or(singleLineComment("--"))
                .or(singleLineComment("#"))
                .or(singleLineComment("//"));
    }

    protected Parser whitespace() {
        return CharacterParser.whitespace().or(ref("comment"));
    }

    protected Parser positiveNumber() {
        return digit().plus();
    }

    protected Parser number() {
        return of("-").optional().seq(ref("positiveNumber"));
    }

    protected Parser color() {
        return newLiterals("Rojo", "Verde", "Azul", "Negro");
    }

    protected Parser direction() {
        return newLiterals("Norte", "Sur", "Este", "Oeste");
    }

    protected Parser booleans() {
        return newLiterals("True", "False");
    }

    protected Parser predefinedProcedures() {
        return newLiterals(
                "Poner",
                "Sacar",
                "Mover",
                "IrAlBorde",
                "VaciarTablero"
        );
    }

    protected Parser predefinedFunctions() {
        return newLiterals(
                "nroBolitas",
                "hayBolitas",
                "puedeMover",
                "siguiente",
                "previo",
                "opuesto",
                "minBool",
                "maxBool",
                "minDir",
                "maxDir",
                "minColor",
                "maxColor"
        );
    }

    protected Parser reservedOp() {
        return newLiterals(
                ":=",
                "..",
                "_",
                "->",
                ";",
                "{",
                "}",
                "[",
                "]",
                "^"
        );
    }

    protected Parser newLiteral(String name) {
        String tokenName = name + "Token";
        def(tokenName, token(name));
        return ref(tokenName);
    }

    protected Parser newLiterals(String... literalList) {
        if (literalList == null || literalList.length < 2) {
            throw new AssertionError("At least 2 Literals options are required");
        }

        List<Parser> literalParsers = new ArrayList<>();

        for (String literalToken : literalList) {
            literalParsers.add(newLiteral(literalToken));
        }

        return new ChoiceParser(literalParsers.toArray(new Parser[literalParsers.size()]));
    }

    private Parser singleLineComment(String symbol) {
        String name = "SingleComment: " + symbol;
        def(name, of(symbol).seq(any().starLazy(of("\n"))).seq(of("\n")));
        return ref(name);
    }

    private Parser multiLineComment(String startSymbol, String endSymbol) {
        String name = "MultiComment: " + startSymbol + " - " + endSymbol;
        def(name, of(startSymbol)
                .seq(any().starLazy(of(endSymbol)))
                .seq(of(endSymbol)));
        return ref(name);
    }

    private Parser token(Object input) {
        Parser parser;
        if (input instanceof Parser) {
            parser = (Parser) input;
        } else if (input instanceof String) {
            String inputAsString = (String) input;
            if (inputAsString.length() == 1) {
                return is(inputAsString.charAt(0));
            } else {
                parser = of(inputAsString);
            }
        } else {
            throw new IllegalStateException("Object not parsable: " + input);
        }
        return parser.token().trim(ref("whitespace"));
    }
}
