package ar.fi.uba.tecnicas.jgobstones;

import org.petitparser.context.Token;
import org.petitparser.parser.Parser;

public class GobstonesEvaluatorExample extends GobstonesGrammar {
    protected Parser color() {
        return super.color().map(match -> {
            Token token = (Token) match;
            return "Color: " + token.getValue();
        });
    }

    @Override
    protected Parser reservedOp() {
        return super.reservedOp().map(match -> {
            Token token = (Token) match;
            return "ReservedOp: '" + token.getValue() + "'";
        });
    }

}
