package jgobstones;

import ar.fi.uba.tecnicas.jgobstones.GobstonesEvaluatorExample;
import org.petitparser.context.ParseError;

public class Playground {

    public static void main(String[] args) {
        GobstonesEvaluatorExample gobstones = new GobstonesEvaluatorExample();
        try {
            Object tree = gobstones.parse("Rojo..RoJJJdo").get();
            System.out.println(tree);
        } catch (ParseError e) {
            System.out.println(e.getFailure().getMessage() + " at: " + e.getFailure().getPosition());;
        }
    }

}
