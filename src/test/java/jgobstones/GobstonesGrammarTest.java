package jgobstones;

import ar.fi.uba.tecnicas.jgobstones.GobstonesGrammar;
import org.junit.Test;
import org.petitparser.context.ParseError;
import org.petitparser.context.Result;
import org.petitparser.parser.Parser;
import org.petitparser.tools.CompositeParser;

import static java.lang.String.format;

public class GobstonesGrammarTest {
    private final CompositeParser gobstones = new GobstonesGrammar();

    private <T> T validate(String source, String production) {
        Parser parser = gobstones.ref(production).end();
        Result result = parser.parse(source);
        try {
            return result.get();
        } catch (ParseError e) {
            int startPosition = e.getFailure().getPosition() - 5 > 0 ? e.getFailure().getPosition() - 5 : 0;
            int endPosition = e.getFailure().getPosition() + 6 < e.getFailure().getBuffer().length() ? e.getFailure().getPosition() + 6 : e.getFailure().getBuffer().length();
            throw new RuntimeException(
                    format("Error parsing: \n%s\n Error near: %s\n%s",
                            e.getFailure().getBuffer(),
                            e.getFailure().getBuffer().substring(startPosition, endPosition),
                            e.getFailure().getMessage()),
                    e);
        }
    }

    @Test
    public void numberLiteral() {
        validate("10", "number");
    }

    @Test
    public void negativeNumberLiteral() {
        validate("-1", "number");
    }

    @Test
    public void haskellMultiComment() {
        validate("{- hello! -}", "comment");
    }

    @Test
    public void javaMultiComment() {
        validate("/* hello! */", "comment");
    }

    @Test
    public void haskellSingleComment() {
        validate("-- hello!\n", "comment");
    }

    @Test
    public void javaSingleComment() {
        validate("// hello! \n", "comment");
    }

    @Test
    public void pythonSingleComment() {
        validate("# hello! \n", "comment");
    }

    @Test
    public void pythonMultiComment() {
        validate("\"\"\" hello! \"\"\"", "comment");
    }

    @Test
    public void booleanLiterals() {
        validate("True", "boolean");
        validate("False", "boolean");
    }

    @Test
    public void colorLiterals() {
        validate("Verde", "color");
        validate("Rojo", "color");
        validate("Azul", "color");
        validate("Negro", "color");
    }

    @Test
    public void directionLiterals() {
        validate("Norte", "direction");
        validate("Este", "direction");
        validate("Sur", "direction");
        validate("Oeste", "direction");
    }

    @Test
    public void predefinedProcedures() {
        validate("Poner", "predefProc");
        validate("Sacar", "predefProc");
        validate("Mover", "predefProc");
        validate("IrAlBorde", "predefProc");
        validate("VaciarTablero", "predefProc");
    }

    @Test
    public void predefinedFunctions() {
        validate("nroBolitas", "predefFunc");
        validate("hayBolitas", "predefFunc");
        validate("puedeMover", "predefFunc");
        validate("siguiente", "predefFunc");
        validate("previo", "predefFunc");
        validate("opuesto", "predefFunc");
        validate("minBool", "predefFunc");
        validate("maxBool", "predefFunc");
        validate("minDir", "predefFunc");
        validate("maxDir", "predefFunc");
        validate("minColor", "predefFunc");
        validate("maxColor", "predefFunc");
    }

    @Test
    public void reservedOp() {
        validate(":=", "reservedOp");
        validate("..", "reservedOp");
        validate("_", "reservedOp");
        validate("->", "reservedOp");
        validate(";", "reservedOp");
        validate("{", "reservedOp");
        validate("}", "reservedOp");
        validate("[", "reservedOp");
        validate("]", "reservedOp");
        validate("^", "reservedOp");
    }

    @Test
    public void literals() {
        validate("True", "literal");
        validate("Verde", "literal");
        validate("Oeste", "literal");
        validate("10", "literal");
        validate("-10", "literal");
    }

    @Test
    public void lowerId() {
        validate("lowerId", "lowerId");
        validate("lower", "lowerId");
    }

    @Test
    public void upperId() {
        validate("UpperId", "upperId");
        validate("UPPERID", "upperId");
        validate("VaciarTablero", "upperId");
    }

    @Test
    public void funcName() {
        validate("funcionName", "funcName");
        validate("anotherfunctionname", "funcName");
    }

    @Test
    public void varTuple() {
        validate("()", "varTuple");
        validate("(varName)", "varTuple");
        validate("(varName1, varName2)", "varTuple");
        validate("(varName1,varName2)", "varTuple");
        validate("(varName1 ,varName2)", "varTuple");
        validate("( varName1 ,varName2 ,lal )", "varTuple");
    }

    @Test
    public void gexp() {
        validate("1 div 2", "gexp");
        validate("1", "gexp");
        validate("1+2", "gexp");
        validate("1/=2", "gexp");
        validate("(5 + 3) - 3", "gexp");
        validate("not True", "gexp");
        validate("f()", "gexp");
        validate("fa( )", "gexp");
        validate("f( 1 )", "gexp");
        validate("f(xa(y(1)))", "gexp");
        validate("f( 1 ,3)", "gexp");
        validate("f( 1 ,3+3)", "gexp");
        validate("f( 1 ,f2(3+3))", "gexp");
        validate("f(1+3)", "gexp");
        validate("fd(Norte,Este,Oeste,12+67+True||   func(3))", "gexp");
        validate("fd(1,2+3,Oeste)", "gexp");
        validate("(3/=2) && 1<2 || (34>=221) && 12 || ((5 + -3) > 3)", "gexp");
        validate("(Oeste/=Este) && Sur<21 ^3|| (-234>=Oeste) && Norte || ((5 + 3) > Sur)", "gexp");
        validate("(Oeste/=Este) && f() && fd(1,2+3,Oeste)|| (Este>=Oeste) && True || ((5 + 3) > 3 + aFunction(1+3))", "gexp");
    }

    @Test
    public void simpleCmd() {
        validate("Skip", "simpleCmd");
        validate("a:=23", "simpleCmd");
        validate("(varName1 ,varName2) := f(1,2)", "simpleCmd");
        validate("PonerLaPutaMadreEsLargo(Verde)", "simpleCmd");
        validate("Sacar ( Verde ) ", "simpleCmd");
        validate("Mover(Oeste)", "simpleCmd");
        validate("IrAlBorde(Oeste)", "simpleCmd");
        validate("VaciarTablero()", "simpleCmd");
        validate("AProc()", "simpleCmd");
        validate("AnotherProcWithParams(Verde, 1+3)", "simpleCmd");
    }

}
